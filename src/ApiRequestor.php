<?php 

namespace FullCycle\ApiFramework;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class ApiRequestor {
    
    private $_apiKey;
    private $_apiBase;
    
    
    function __construct($_apiKey = null, $_apiBase = null) {
        $this->_apiKey = $_apiKey;
        $this->_apiBase = $_apiBase;
        
    }

    function openClient() {
        $this->client = new Client();
    }
    
    function request($method, $url, $params = null, $headers = null) {
        $params = $params ?: [];
        $headers = $headers ?: [];
        
        return $this->_requestRaw($method,$url,$params,$headers);
    }
    
    private function _requestRaw($method, $url, $params, $headers)
    {
        
//        $this->client = new Client();
	$this->openClient();
        $method = strtoupper($method);
	$opts = [];
	if ($method == "GET") {
		$opts['query'] = $params;
	} else {
		if (!empty($params)) {	// This needs to be smarter here for if we need to post json or not
		        $opts['json'] =$params;
		}
	}
//        $opts['debug'] = true;
//      print_r($opts); 
        try {
            $resp = $this->client->request($method,$url,$opts);
        } catch (\Exception $e) {
                echo "Exception: {$e->getMessage()}\n";
                //echo $e->getResponse()->getBody();
                throw $e;
        }
        return $resp;
    }
    
    private function _defaultHeaders($apiKey) {
        $defaultHeaders = [
 //           'Token' => $apiKey,
            'Authorization' => "OAuth {$apiKey}",
        ];
        return $defaultHeaders;
        
    }
    
}

