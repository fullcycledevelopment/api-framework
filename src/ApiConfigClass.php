<?php

namespace FullCycle\ApiFramework;

class ApiConfigClass {
	static protected $apiBase = false;
	static protected $apiAccessKey = false;
	static $_requestor="\FullCycle\ApiFramework\ApiRequestor";

	static public function setApiAccessKey($key) {
		static::$apiAccessKey=$key;	
	}

        static public function getApiBase() {
            return static::$apiBase;
        }

        static public function getApiAccessKey() {
            return static::$apiAccessKey;
        }
	
	static function getRequestor() {
		return static::$_requestor;
	}

	static function setRequestor($value) {
		static::$_requestor = $value;
	}

}



