#APIFramework



#Make your config object.

```
<?php

use FullCycle\ApiFramework\ApiConfigClass;
use \Exception;


class YourApiConfig extends ApiConfigClass {

...
}

```

#Make your base object

```
<?php


use FullCycle\ApiFramework\ApiObject


class YourObject extends ApiObject {


}
```
#Make your resource class

```
class YourResource extends ApiResource {

...

}

```



